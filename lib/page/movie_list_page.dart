import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:get_it/get_it.dart';
import 'package:zulfi_briix/model/movie_list.dart';
import 'package:zulfi_briix/route/app_router.dart';
import '../components/card/card_movie.dart';
import '../constant/color.dart';
import '../model/movie_form.dart';

@RoutePage()
class MovieListPage extends StatefulWidget {
  MovieListPage({super.key});

  @override
  State<MovieListPage> createState() => _MovieListPageState();
}

class _MovieListPageState extends State<MovieListPage> {
  final MovieForm movieForm = GetIt.instance<MovieForm>();
  final MovieList movieList = GetIt.instance<MovieList>();

  TextEditingController search = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "List Movies",
          style: TextStyle(
            fontSize: 20,
            color: Colors.white,
          ),
        ),
        backgroundColor: colorPrimary,
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.symmetric(horizontal: 24, vertical: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TextField(
              controller: search,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'Search By Name',
                contentPadding: EdgeInsets.symmetric(
                  horizontal: 16,
                  vertical: 8,
                ),
              ),
              onChanged: (v) {
                movieList.changeSearch(v);
              },
            ),
            SizedBox(
              height: 24,
            ),
            Observer(
              builder: (ctx) => movieList.filteredDatas.length > 0
                  ? Observer(
                      builder: (ctx) => ListView.separated(
                        itemCount: movieList.filteredDatas.length,
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        separatorBuilder: (context, index) {
                          return SizedBox(
                            height: 24,
                          );
                        },
                        itemBuilder: (BuildContext context, int index) {
                          return CardMovie(
                            movie: movieList.filteredDatas[index],
                            onClick: () {
                              movieForm.setCurrentMovie(
                                  movieList.filteredDatas[index]);
                              AutoRouter.of(context).push(MovieFormRoute());
                            },
                          );
                        },
                      ),
                    )
                  : Center(
                      child: Column(
                        children: [
                          Icon(
                            Icons.hourglass_empty_rounded,
                            size: 40,
                            color: colorPrimary,
                          ),
                          SizedBox(
                            height: 16,
                          ),
                          Text("Sorry we don't have any movie"),
                        ],
                      ),
                    ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: colorPrimary,
        foregroundColor: Colors.white,
        onPressed: () {
          AutoRouter.of(context).push(MovieFormRoute());
        },
        child: Icon(Icons.add),
      ),
    );
  }
}
