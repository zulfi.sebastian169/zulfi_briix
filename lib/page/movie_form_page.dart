import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:get_it/get_it.dart';
import 'package:zulfi_briix/constant/color.dart';
import 'package:zulfi_briix/model/movie_form.dart';

import '../components/bottom_sheet/delete_movie.dart';
import '../components/card/card_genre.dart';
import '../constant/genre.dart';
import '../model/movie_list.dart';

@RoutePage()
class MovieFormPage extends StatefulWidget {
  MovieFormPage({
    super.key,
  });

  @override
  State<MovieFormPage> createState() => _MovieFormPageState();
}

class _MovieFormPageState extends State<MovieFormPage> {
  final MovieForm movie = GetIt.instance<MovieForm>();
  final MovieList listMovie = GetIt.instance<MovieList>();

  TextEditingController titleController = TextEditingController();
  TextEditingController directorController = TextEditingController();
  TextEditingController summaryController = TextEditingController();

  @override
  void initState() {
    titleController.text = movie.currMovie.title;
    directorController.text = movie.currMovie.director;
    summaryController.text = movie.currMovie.summary;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Form Movies",
          style: TextStyle(
            fontSize: 20,
            color: Colors.white,
          ),
        ),
        titleSpacing: 0,
        leading: GestureDetector(
          onTap: () {
            movie.clearCurrentMovie();
            AutoRouter.of(context).pop();
          },
          child: Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 10.0),
            child: Row(
              children: [
                Observer(
                  builder: (ctx) => movie.currMovie.id == 0
                      ? SizedBox()
                      : GestureDetector(
                          onTap: () {
                            showModalBottomSheet(
                              context: context,
                              builder: (BuildContext ctx) {
                                return DeleteMovie(
                                  onCancel: () {
                                    AutoRouter.of(context).pop();
                                  },
                                  onTap: () {
                                    listMovie.deleteMovie(movie.currMovie.id);
                                    ScaffoldMessenger.of(context)
                                        .showSnackBar(const SnackBar(
                                      content: Text("Success Delete Movie"),
                                    ));
                                    movie.clearCurrentMovie();
                                    AutoRouter.of(context).popUntilRoot();
                                  },
                                );
                              },
                            );
                          },
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Icon(
                              Icons.delete,
                              color: Colors.white,
                            ),
                          ),
                        ),
                ),
                Observer(
                  builder: (ctx) => GestureDetector(
                    onTap: () {
                      if (movie.currMovie.id == 0) {
                        listMovie.postMovie(movie.currMovie);
                        ScaffoldMessenger.of(context)
                            .showSnackBar(const SnackBar(
                          content: Text("Success Add Movie"),
                        ));
                        movie.clearCurrentMovie();
                        AutoRouter.of(context).pop();
                      } else {
                        listMovie.updateMovie(movie.currMovie);
                        ScaffoldMessenger.of(context)
                            .showSnackBar(const SnackBar(
                          content: Text("Success Edit Movie"),
                        ));
                        movie.clearCurrentMovie();
                        AutoRouter.of(context).pop();
                      }
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Icon(
                        movie.currMovie.id == 0 ? Icons.add_circle : Icons.edit,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
        backgroundColor: colorPrimary,
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.symmetric(
          horizontal: 16,
          vertical: 16,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Title",
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(
              height: 8,
            ),
            TextField(
              controller: titleController,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'Enter a title',
                contentPadding: EdgeInsets.symmetric(
                  horizontal: 16,
                  vertical: 8,
                ),
              ),
              onChanged: (v) {
                movie.changeTitle(v);
              },
            ),
            SizedBox(
              height: 24,
            ),
            Text(
              "Director",
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(
              height: 8,
            ),
            TextField(
              controller: directorController,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'Enter a director name',
                contentPadding: EdgeInsets.symmetric(
                  horizontal: 16,
                  vertical: 8,
                ),
              ),
              onChanged: (v) {
                movie.changeDirector(v);
              },
            ),
            SizedBox(
              height: 24,
            ),
            Text(
              "Summary",
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(
              height: 8,
            ),
            TextField(
              controller: summaryController,
              keyboardType: TextInputType.multiline,
              maxLines: 3,
              minLines: 3,
              maxLength: 100,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'Input summary',
                contentPadding: EdgeInsets.symmetric(
                  horizontal: 16,
                  vertical: 8,
                ),
              ),
              onChanged: (v) {
                movie.changeSummary(v);
              },
            ),
            SizedBox(
              height: 24,
            ),
            Text(
              "Genre",
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold,
              ),
            ),
            Observer(
              builder: (ctx) => Wrap(
                spacing: 6.0,
                runSpacing: 6.0,
                children: listGenre
                    .map(
                      (_data) => CardGenre(
                        label: _data,
                        isSelected: movie.currMovie.genres.contains(_data),
                        showAvatar: true,
                        onClick: () {
                          movie.changeSelectedGenres(_data);
                        },
                      ),
                    )
                    .toList(),
              ),
            )
          ],
        ),
      ),
    );
  }
}
