import 'package:flutter/material.dart';

import '../../constant/color.dart';

class CardGenre extends StatelessWidget {
  final String label;
  final bool isSelected;
  final bool showAvatar;
  final Function() onClick;

  const CardGenre({
    super.key,
    required this.label,
    required this.isSelected,
    required this.showAvatar,
    required this.onClick,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onClick,
      child: Chip(
        labelPadding: EdgeInsets.symmetric(horizontal: 8.0),
        avatar: showAvatar
            ? CircleAvatar(
                backgroundColor: Colors.white70,
                child: Text(
                  label[0].toUpperCase(),
                ),
              )
            : null,
        label: Text(
          "${label[0].toUpperCase()}${label.substring(1).toLowerCase()}",
          style: TextStyle(
            color: isSelected ? Colors.white : Colors.black,
            fontSize: 12,
          ),
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
          side: BorderSide(
            color: Color(0xffe2e2e2),
          ),
        ),
        backgroundColor: isSelected ? colorPrimary : Color(0xffdbdbdb),
        elevation: 0.0,
        padding: EdgeInsets.all(5.0),
      ),
    );
  }
}
