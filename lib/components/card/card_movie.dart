import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:zulfi_briix/model/movie.dart';

import '../../constant/color.dart';
import 'card_genre.dart';

class CardMovie extends StatelessWidget {
  final Movie movie;
  final Function() onClick;

  const CardMovie({
    super.key,
    required this.onClick,
    required this.movie,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: 16,
        vertical: 12,
      ),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
        border: Border.all(
          width: 1,
          color: colorPrimary,
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  Icon(
                    Icons.movie_outlined,
                    size: 32,
                    color: colorPrimary,
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  Observer(
                    builder: (ctx) => Text(
                      movie.title,
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                        letterSpacing: 1,
                      ),
                    ),
                  ),
                ],
              ),
              GestureDetector(
                onTap: () {
                  onClick();
                },
                child: Container(
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: colorPrimary,
                  ),
                  padding: EdgeInsets.all(4),
                  child: Icon(
                    Icons.mode_edit_outline_outlined,
                    size: 16,
                    color: Colors.white,
                  ),
                ),
              ),
            ],
          ),
          Observer(
            builder: (ctx) => Text(
              "Directed By : " + movie.director,
              style: TextStyle(
                fontSize: 14,
                fontStyle: FontStyle.italic,
              ),
            ),
          ),
          SizedBox(
            height: 4,
          ),
          Observer(
            builder: (ctx) => Wrap(
              spacing: 2.0,
              runSpacing: 2.0,
              children: movie.genres
                  .map(
                    (_data) => CardGenre(
                      label: _data,
                      isSelected: true,
                      showAvatar: false,
                      onClick: () {},
                    ),
                  )
                  .toList(),
            ),
          ),
          SizedBox(
            height: 4,
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.symmetric(
              horizontal: 8,
              vertical: 8,
            ),
            decoration: BoxDecoration(
              color: Color(0xfff2f2f2),
              borderRadius: BorderRadius.circular(10),
            ),
            child: Observer(
              builder: (ctx) => Text(
                movie.summary,
                style: TextStyle(
                  fontSize: 14,
                  color: Colors.black87,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
