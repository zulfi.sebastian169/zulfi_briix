import 'package:mobx/mobx.dart';
import 'package:zulfi_briix/model/movie.dart';

part "movie_form.g.dart";

class MovieForm = _MovieFormBase with _$MovieForm;

abstract class _MovieFormBase with Store {
  @observable
  Movie currMovie = Movie(
      id: 0,
      title: "",
      director: "",
      summary: "",
      genres: <String>[].asObservable());

  @action
  void setCurrentMovie(_movie) {
    currMovie = _movie;
  }

  @action
  void clearCurrentMovie() {
    currMovie = Movie(
      id: 0,
      title: "",
      director: "",
      summary: "",
      genres: <String>[].asObservable(),
    );
  }

  @action
  void changeTitle(_data) {
    currMovie.title = _data;
  }

  @action
  void changeDirector(_data) {
    currMovie.director = _data;
  }

  @action
  void changeSummary(_data) {
    currMovie.summary = _data;
  }

  @action
  void changeSelectedGenres(_newGenre) {
    if (!currMovie.genres.contains(_newGenre)) {
      currMovie.genres.add(_newGenre);
    } else {
      currMovie.genres.remove(_newGenre);
    }
  }
}
