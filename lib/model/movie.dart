import 'package:mobx/mobx.dart';

part "movie.g.dart";

class Movie = _MovieBase with _$Movie;

abstract class _MovieBase with Store {
  final num id;

  @observable
  String title;

  @observable
  String director;

  @observable
  String summary;

  @observable
  ObservableList<String> genres;

  _MovieBase({
    required this.id,
    required this.title,
    required this.director,
    required this.summary,
    required this.genres,
  });
}
