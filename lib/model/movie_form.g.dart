// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'movie_form.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$MovieForm on _MovieFormBase, Store {
  late final _$currMovieAtom =
      Atom(name: '_MovieFormBase.currMovie', context: context);

  @override
  Movie get currMovie {
    _$currMovieAtom.reportRead();
    return super.currMovie;
  }

  @override
  set currMovie(Movie value) {
    _$currMovieAtom.reportWrite(value, super.currMovie, () {
      super.currMovie = value;
    });
  }

  late final _$_MovieFormBaseActionController =
      ActionController(name: '_MovieFormBase', context: context);

  @override
  void setCurrentMovie(dynamic _movie) {
    final _$actionInfo = _$_MovieFormBaseActionController.startAction(
        name: '_MovieFormBase.setCurrentMovie');
    try {
      return super.setCurrentMovie(_movie);
    } finally {
      _$_MovieFormBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void clearCurrentMovie() {
    final _$actionInfo = _$_MovieFormBaseActionController.startAction(
        name: '_MovieFormBase.clearCurrentMovie');
    try {
      return super.clearCurrentMovie();
    } finally {
      _$_MovieFormBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void changeTitle(dynamic _data) {
    final _$actionInfo = _$_MovieFormBaseActionController.startAction(
        name: '_MovieFormBase.changeTitle');
    try {
      return super.changeTitle(_data);
    } finally {
      _$_MovieFormBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void changeDirector(dynamic _data) {
    final _$actionInfo = _$_MovieFormBaseActionController.startAction(
        name: '_MovieFormBase.changeDirector');
    try {
      return super.changeDirector(_data);
    } finally {
      _$_MovieFormBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void changeSummary(dynamic _data) {
    final _$actionInfo = _$_MovieFormBaseActionController.startAction(
        name: '_MovieFormBase.changeSummary');
    try {
      return super.changeSummary(_data);
    } finally {
      _$_MovieFormBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void changeSelectedGenres(dynamic _newGenre) {
    final _$actionInfo = _$_MovieFormBaseActionController.startAction(
        name: '_MovieFormBase.changeSelectedGenres');
    try {
      return super.changeSelectedGenres(_newGenre);
    } finally {
      _$_MovieFormBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
currMovie: ${currMovie}
    ''';
  }
}
