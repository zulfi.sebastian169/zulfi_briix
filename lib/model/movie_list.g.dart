// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'movie_list.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$MovieList on _MovieListBase, Store {
  late final _$datasAtom = Atom(name: '_MovieListBase.datas', context: context);

  @override
  ObservableList<Movie> get datas {
    _$datasAtom.reportRead();
    return super.datas;
  }

  @override
  set datas(ObservableList<Movie> value) {
    _$datasAtom.reportWrite(value, super.datas, () {
      super.datas = value;
    });
  }

  late final _$filteredDatasAtom =
      Atom(name: '_MovieListBase.filteredDatas', context: context);

  @override
  ObservableList<Movie> get filteredDatas {
    _$filteredDatasAtom.reportRead();
    return super.filteredDatas;
  }

  @override
  set filteredDatas(ObservableList<Movie> value) {
    _$filteredDatasAtom.reportWrite(value, super.filteredDatas, () {
      super.filteredDatas = value;
    });
  }

  late final _$_MovieListBaseActionController =
      ActionController(name: '_MovieListBase', context: context);

  @override
  void changeSearch(dynamic _search) {
    final _$actionInfo = _$_MovieListBaseActionController.startAction(
        name: '_MovieListBase.changeSearch');
    try {
      return super.changeSearch(_search);
    } finally {
      _$_MovieListBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void postMovie(Movie _movie) {
    final _$actionInfo = _$_MovieListBaseActionController.startAction(
        name: '_MovieListBase.postMovie');
    try {
      return super.postMovie(_movie);
    } finally {
      _$_MovieListBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void updateMovie(Movie _data) {
    final _$actionInfo = _$_MovieListBaseActionController.startAction(
        name: '_MovieListBase.updateMovie');
    try {
      return super.updateMovie(_data);
    } finally {
      _$_MovieListBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void deleteMovie(dynamic _id) {
    final _$actionInfo = _$_MovieListBaseActionController.startAction(
        name: '_MovieListBase.deleteMovie');
    try {
      return super.deleteMovie(_id);
    } finally {
      _$_MovieListBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
datas: ${datas},
filteredDatas: ${filteredDatas}
    ''';
  }
}
