import 'dart:math';

import 'package:mobx/mobx.dart';
import 'package:zulfi_briix/model/movie.dart';

part "movie_list.g.dart";

class MovieList = _MovieListBase with _$MovieList;

abstract class _MovieListBase with Store {
  @observable
  ObservableList<Movie> datas = ObservableList<Movie>();

  @observable
  ObservableList<Movie> filteredDatas = ObservableList<Movie>();

  @action
  void changeSearch(_search) {
    this.filteredDatas = datas
        .where((e) => e.title.toLowerCase().contains(_search.toLowerCase()))
        .toList()
        .asObservable();
  }

  @action
  void postMovie(Movie _movie) {
    var _id = Random().nextInt(10000000) + datas.length;
    datas.add(
      Movie(
        id: _id,
        title: _movie.title,
        director: _movie.director,
        summary: _movie.summary,
        genres: _movie.genres,
      ),
    );
    this.filteredDatas = datas;
  }

  @action
  void updateMovie(Movie _data) {
    var _index = this.datas.indexWhere((e) => e.id == _data.id);
    Movie _movie = this.datas[_index];
    _movie.title = _data.title;
    _movie.director = _data.director;
    _movie.summary = _data.summary;
    _movie.genres = _data.genres;
    this.datas[_index] = _movie;
    this.filteredDatas = datas;
  }

  @action
  void deleteMovie(_id) {
    this.datas.removeWhere((e) => e.id == _id);
    this.filteredDatas = datas;
  }
}
