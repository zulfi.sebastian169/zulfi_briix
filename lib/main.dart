import 'package:flutter/material.dart';
import 'package:zulfi_briix/locator.dart';
import 'package:zulfi_briix/route/app_router.dart';

void main() {
  setup();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({super.key});

  final _appRouter = AppRouter();

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      debugShowCheckedModeBanner: false,
      title: 'Movie App',
      routerConfig: _appRouter.config(),
    );
  }
}
