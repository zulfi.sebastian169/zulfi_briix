import 'package:get_it/get_it.dart';
import 'package:zulfi_briix/model/movie_form.dart';
import 'package:zulfi_briix/model/movie_list.dart';

final locator = GetIt.instance;

void setup() {
  locator.registerLazySingleton<MovieList>(() => MovieList());
  locator.registerLazySingleton<MovieForm>(() => MovieForm());
}
