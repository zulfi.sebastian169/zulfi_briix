// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouterGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

part of 'app_router.dart';

abstract class _$AppRouter extends RootStackRouter {
  // ignore: unused_element
  _$AppRouter({super.navigatorKey});

  @override
  final Map<String, PageFactory> pagesMap = {
    MovieFormRoute.name: (routeData) {
      final args = routeData.argsAs<MovieFormRouteArgs>(
          orElse: () => const MovieFormRouteArgs());
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: MovieFormPage(key: args.key),
      );
    },
    MovieListRoute.name: (routeData) {
      final args = routeData.argsAs<MovieListRouteArgs>(
          orElse: () => const MovieListRouteArgs());
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: MovieListPage(key: args.key),
      );
    },
  };
}

/// generated route for
/// [MovieFormPage]
class MovieFormRoute extends PageRouteInfo<MovieFormRouteArgs> {
  MovieFormRoute({
    Key? key,
    List<PageRouteInfo>? children,
  }) : super(
          MovieFormRoute.name,
          args: MovieFormRouteArgs(key: key),
          initialChildren: children,
        );

  static const String name = 'MovieFormRoute';

  static const PageInfo<MovieFormRouteArgs> page =
      PageInfo<MovieFormRouteArgs>(name);
}

class MovieFormRouteArgs {
  const MovieFormRouteArgs({this.key});

  final Key? key;

  @override
  String toString() {
    return 'MovieFormRouteArgs{key: $key}';
  }
}

/// generated route for
/// [MovieListPage]
class MovieListRoute extends PageRouteInfo<MovieListRouteArgs> {
  MovieListRoute({
    Key? key,
    List<PageRouteInfo>? children,
  }) : super(
          MovieListRoute.name,
          args: MovieListRouteArgs(key: key),
          initialChildren: children,
        );

  static const String name = 'MovieListRoute';

  static const PageInfo<MovieListRouteArgs> page =
      PageInfo<MovieListRouteArgs>(name);
}

class MovieListRouteArgs {
  const MovieListRouteArgs({this.key});

  final Key? key;

  @override
  String toString() {
    return 'MovieListRouteArgs{key: $key}';
  }
}
