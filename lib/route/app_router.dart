import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import '../page/movie_form_page.dart';
import '../page/movie_list_page.dart';

part 'app_router.gr.dart';

@AutoRouterConfig(replaceInRouteName: 'Page,Route')
class AppRouter extends _$AppRouter {
  @override
  List<AutoRoute> get routes => [
        AutoRoute(page: MovieListRoute.page, initial: true),
        AutoRoute(page: MovieFormRoute.page),
      ];
}
